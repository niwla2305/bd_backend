from flask_restful import Resource
from flask_jwt_extended import jwt_required
from app.database import ratings
from marshmallow import ValidationError
from .models import RateStudentSchema
from flask import request


class RateStudent(Resource):
    @jwt_required
    def post(self, student_id):
        try:
            data = RateStudentSchema().load(request.json)
            ratings.update_one({"id": student_id}, {"$set": {"rating": data["rating"]}})
            return {"msg": "ok"}
        except ValidationError as err:
            return err.messages, 400
