from marshmallow import Schema, fields


class AddStudentSchema(Schema):
    name = fields.Str(required=True)
