from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource
from marshmallow import ValidationError
from app.database import classes
from .models import EditStudentSchema


class Student(Resource):
    @jwt_required
    def delete(self, class_id, student_id):
        class_ = classes.find_one({"id": class_id})
        if not class_:
            return {"msg": "no such student"}, 404
        new_students = [x for x in class_["students"] if not (student_id == x.get('id'))]
        classes.update_one({"id": class_id}, {"$set": {"students": new_students}})
        return {"msg": "deleted"}, 200

    @jwt_required
    def put(self, class_id, student_id):
        try:
            data = EditStudentSchema().load(request.json)
            class_ = classes.find_one({"id": class_id})
            if not class_:
                return {"msg": "no such student"}, 404
            edited_student = [x for x in class_["students"] if (student_id == x.get('id'))][0]
            edited_student["name"] = data["name"]
            new_students = [x for x in class_["students"] if not (student_id == x.get('id'))]
            new_students.append(edited_student)
            new_students = sorted(new_students, key=lambda k: k["name"])
            classes.update_one({"id": class_id}, {"$set": {"students": new_students}})
            return {"msg": "deleted"}, 200

        except ValidationError as err:
            return err.messages, 400
