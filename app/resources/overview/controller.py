from flask_restful import Resource
from flask_jwt_extended import jwt_required
from app.database import ratings


class Overview(Resource):
    def get(self):
        ratings_json = []
        for document in ratings.find():
            ratings_json.append({"id": document["id"], "name": document["name"], "rating": document["rating"]})
        return ratings_json
