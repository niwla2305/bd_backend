from flask_jwt_extended import jwt_required
from flask_restful import Resource
import os
import threading


class System(Resource):
    def shutdown(self):
        os.system("shutdown now")

    def reboot(self):
        os.system("reboot")

    def update(self):
        script_path = os.environ.get("BD_UPDATE_SCRIPT") or\
                      f"{'/'.join(os.path.realpath(__file__).split('/')[:-5])}/tools/update.sh"
        os.system(script_path)

    @jwt_required
    def post(self, command):
        print(command)
        if command == "poweroff":
            threading.Thread(target=self.shutdown).run()
            return {"msg": "ok"}
        elif command == "reboot":
            threading.Thread(target=self.reboot).run()
            return {"msg": "ok"}
        elif command == "update":
            threading.Thread(target=self.update).run()
            return {"msg": "ok"}
