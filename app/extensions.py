"""Extensions registry

All extensions here are used as singletons and
initialized in application factory
"""
from flask_jwt_extended import JWTManager
from flask_marshmallow import Marshmallow
from flask_restful import Api

jwt = JWTManager()
ma = Marshmallow()
rest = Api()
