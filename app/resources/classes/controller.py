from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource
from marshmallow import ValidationError
import uuid
from app.database import classes
from .models import AddClassSchema


class Classes(Resource):
    @jwt_required
    def get(self):
        classes_json = []
        for document in classes.find():
            classes_json.append({"name": document["name"], "id": str(document["id"])})
        return classes_json

    @jwt_required
    def post(self):
        try:
            data = AddClassSchema().load(request.json)
            db_document = {"name": data["name"], "id": str(uuid.uuid4())}
            if "students" in data.keys():
                db_document["students"] = data["students"]
            else:
                db_document["students"] = []
            classes.insert_one(db_document)
            return {"msg": "ok"}
        except ValidationError as err:
            return err.messages, 400
