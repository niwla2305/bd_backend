from marshmallow import Schema, fields


class LoadClassSchema(Schema):
    id = fields.UUID(required=True)
