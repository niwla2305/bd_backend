from marshmallow import Schema, fields, validates, ValidationError


class RateStudentSchema(Schema):
    rating = fields.Integer(required=True)

    @validates("rating")
    def validate_quantity(self, value):
        if value < 0:
            raise ValidationError("Rating must be greater than 0.")
        if value > 3:
            raise ValidationError("Rating must not be greater than 3.")
