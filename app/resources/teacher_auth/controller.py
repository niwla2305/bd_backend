import secrets

from flask_restful import Resource
from flask import request
from app.extensions import jwt
from flask_jwt_extended import create_access_token
import os


class TeacherAuth(Resource):
    def get(self):
        return {"hi": "alöwi"}

    def post(self):
        if request.json["password"] == os.environ.get("BD_PASSWORD"):
            token = create_access_token(identity="teacher")
            return {"token": token}
        else:
            return {"msg": "login failed"}, 401
