from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource


class Me(Resource):
    @jwt_required
    def get(self):
        return {"user": get_jwt_identity()}
