from marshmallow import Schema, fields


class AddClassSchema(Schema):
    name = fields.Str(required=True)
    students = fields.List(fields.Str())

