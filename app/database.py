import pymongo

mongo_client = pymongo.MongoClient()
db = mongo_client["behaviour_display"]
ratings = db["ratings"]
classes = db["classes"]
