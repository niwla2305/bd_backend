"""Default configuration

Use env var to override
"""
import os
import secrets

ENV = os.getenv("FLASK_ENV")
DEBUG = ENV == "development"
SECRET_KEY = secrets.token_hex()
PROPAGATE_EXCEPTIONS = True
