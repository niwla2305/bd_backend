import uuid

from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource
from marshmallow import ValidationError

from app.database import classes
from .models import AddStudentSchema


class Class(Resource):
    @jwt_required
    def get(self, class_id):
        db_result = classes.find_one({"id": class_id})
        response = {
            "id": str(db_result["id"]),
            "name": db_result["name"]
        }
        if "students" in db_result.keys():
            response["students"] = db_result["students"]
        else:
            response["students"] = []
        return response

    @jwt_required
    def post(self, class_id):
        try:
            document = classes.find_one({"id": class_id})
            data = AddStudentSchema().load(request.json)
            db_student = {"name": data["name"], "id": str(uuid.uuid4())}
            document["students"].append(db_student)
            classes.replace_one({"id": class_id}, document)
            return {"msg": "ok"}
        except ValidationError as err:
            return err.messages, 400

    @jwt_required
    def delete(self, class_id):
        result = classes.delete_one({"id": class_id})
        if result.deleted_count > 0:
            return {"msg": "deleted"}, 200
        else:
            return {"msg": "no such student"}, 404
