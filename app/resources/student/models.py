from marshmallow import Schema, fields


class EditStudentSchema(Schema):
    name = fields.Str(required=True)
