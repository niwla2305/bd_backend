from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource
from marshmallow import ValidationError

from app.database import classes, ratings
from .models import LoadClassSchema


class LoadClass(Resource):
    @jwt_required
    def post(self):
        try:
            data = LoadClassSchema().load(request.json)
            class_ = classes.find_one({"id": str(data['id'])})
            students = []
            for student in class_["students"]:
                students.append({"id": student['id'], "name": student['name'], "rating": 1})
            ratings.remove()
            ratings.insert_many(students)
            return {"msg": "ok"}
        except ValidationError as err:
            return err.messages, 400
