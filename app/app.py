from flask import Flask

from app.extensions import jwt, ma, rest

from app.resources.teacher_auth.controller import TeacherAuth
from app.resources.overview.controller import Overview
from app.resources.classes.controller import Classes
from app.resources.class_.controller import Class
from app.resources.student.controller import Student
from app.resources.load_class.controller import LoadClass
from app.resources.rate.controller import RateStudent
from app.resources.me.controller import Me
from app.resources.system.controller import System


def create_app(testing=False, cli=False):
    """Application factory, used to create application"""
    app = Flask("app")
    app.config.from_object("app.config")

    if testing is True:
        app.config["TESTING"] = True
    register_resources(rest)
    configure_extensions(app, cli)

    return app


def configure_extensions(app, cli):
    """configure flask extensions"""
    jwt.init_app(app)
    ma.init_app(app)
    rest.init_app(app)


def register_resources(api):
    """register all blueprints for application"""
    api.add_resource(TeacherAuth, "/admin/auth")
    api.add_resource(Classes, "/admin/classes")
    api.add_resource(Class, "/admin/classes/<string:class_id>")
    api.add_resource(Student, "/admin/classes/<string:class_id>/<string:student_id>")
    api.add_resource(Overview, "/overview")
    api.add_resource(LoadClass, "/admin/loadClass")
    api.add_resource(RateStudent, "/admin/rate/<string:student_id>")
    api.add_resource(Me, "/admin/me")
    api.add_resource(System, "/admin/system/<string:command>")
