from dotenv import load_dotenv

from app.app import create_app

load_dotenv()

application = create_app()

if __name__ == '__main__':
    print("THIS IS A DEVELOPMENT SERVER. USE A PRODUCTION WSGI SERVER")
    application.run()
